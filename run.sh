sudo docker kill $(sudo docker ps -a -q)
sudo docker rm $(sudo docker ps -a -q)
sudo docker volume rm $(sudo docker volume ls -qf dangling=true | xargs)
sudo docker-compose build
sudo docker-compose up