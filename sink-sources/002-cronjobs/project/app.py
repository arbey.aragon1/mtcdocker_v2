import os
from flask import Flask, redirect, url_for, request, render_template
from pymongo import MongoClient

app = Flask(__name__)

client = MongoClient(os.environ['DB'])

mtcdb = client["mtc"]
oanda_wti_price = mtcdb["oanda_wti_price"]

item_doc = {
    'name': 'name',
    'description': 'description'
}
oanda_wti_price.insert_one(item_doc)

@app.route('/')
def todo():
    return 'Oanda service'


#@app.route('/new', methods=['POST'])
#def new():
#
#    item_doc = {
#        'name': request.form['name'],
#        'description': request.form['description']
#    }
#    db.tododb.insert_one(item_doc)
#
#    return redirect(url_for('todo'))

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)