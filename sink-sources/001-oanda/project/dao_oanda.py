import sys
import pika
import json
import time

class DAOOanda():
    __connection = None
    __channel = None
    
    def __init__(self):
        self.__connectionCreate()
        
    def __connectionCreate(self):
        for i in range(3):
            try:
                self.__connection = pika.BlockingConnection(
                    pika.ConnectionParameters(
                        host='rabbitmq',                      
                        port=5672,
                        credentials=pika.PlainCredentials(
                            username="admin", password="mypass")
                    )
                )
                break
            except:
                self.__connection = None
                print('Hubo un error en la conexion '+str(i))
                time.sleep(2)
                
        if(self.__connection == None):
            print('Tercer intento fallido')
            sys.exit()
        self.__channel = self.__connection.channel()
        print('Conexion creada')
        
    def updateMarket(self, data):
        message = json.dumps(data)
        self.__channel.queue_declare(queue='hello')
        self.__channel.basic_publish(
            exchange='',
            routing_key='hello',
            body=message)
        
    def connectionClose(self):
        self.__connection.close()