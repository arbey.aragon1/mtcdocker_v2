import os
from flask import Flask, redirect, url_for, request, render_template
from pymongo import MongoClient
from datetime import datetime
from market_factory import MarketFactory
from dao_oanda import DAOOanda
import pika
import time

if __name__ == "__main__":
    client = MongoClient(os.environ['DB'])
    mtcdb = client["mtc"]
    oanda_wti_price = mtcdb["oanda_wti_price"]
    oanda_wti_current_price = mtcdb["oanda_wti_current_price"]
    source = DAOOanda()
    
    print('Paso1')
    marketFactory = MarketFactory.getInstance({})
    print('Paso2')
    newFlow = marketFactory.GET_STREAM({})
    print('Paso3')
    
    def func(s):
        #print('---------------')
        #print(s)
        source.updateMarket(s)
        #oanda_wti_price.insert_one(s)

    print('Paso4')
    newFlow.subscribe(lambda s: func(s))
    while(True):
        time.sleep(60)
