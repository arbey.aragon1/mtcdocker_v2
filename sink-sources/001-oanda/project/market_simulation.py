from market_interface import MarketInterface
from simulation_streams import streamRandomWalk, streamHistData
from datetime import datetime

class MarketSimulation(MarketInterface):
    __instance = None

    def __init__(self, config):
        if MarketSimulation.__instance != None:
            raise Exception("Esta clase no es singleton!")
        else:
            MarketSimulation.__instance = self

    @staticmethod
    def getInstance(config):
        print('*****************')
        print(config)
        print('---')
        if MarketSimulation.__instance == None:
            MarketSimulation(config)
        return MarketSimulation.__instance 
    
    def GET_STREAM(self, configST):
        if(True):
            print('GET_STREAM')
            return streamRandomWalk(
                mlSeconds=1000, 
                initVal=50, 
                mu=1, 
                sigma=0.5,
                limitData=9999).map(lambda s: {
                    'time': datetime.now().strftime('%Y-%m-%dT%H:%M:%S.%f'),
                    'time2': datetime.now().strftime('%Y-%m-%dT%H:%M:%S.%f'), 
                    'bid': s['price'], 
                    'ask': s['price'],
                    'price': s['price'],
                    'day': datetime.now().day
                })
        else:
            return streamHistData(
                fileName='data.csv')

    def BUY(self, order):
        print('+++++++++++++++++')
        pass

    def SELL(self, order):
        print('-----------------')
        pass