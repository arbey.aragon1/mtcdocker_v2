import logging

class LOGGER():
    __mtcdb = None
    __node = None
    __config = None
    __instance = None
    __logger = None

    def __init__(self):
        if LOGGER.__instance != None:
            raise Exception("Esta clase no es singleton!")
        else:
            LOGGER.__instance = self
            logging.basicConfig(filename='../project/log.log',filemode='w',level=logging.DEBUG)

    @staticmethod
    def getInstance():
        print("storage getIns")
        if LOGGER.__instance == None:
            LOGGER()
        return LOGGER.__instance 

    def debug(self, *argv):
        logging.debug(argv)