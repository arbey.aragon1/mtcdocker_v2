import sys
sys.path.append('../shared')
from constants import LONG, SHORT, ANY, BUY, SELL
from order_db_service import OrderDB as storage
from logger import LOGGER

class AlgorithmInterface(object):
    __config = None
    __sink = None
    
    __temporalOrder = None
    __dao = None
    logging = None

    def __init__(self, config, sink, configDB = None, marketConfig = None):
        self.logging = LOGGER.getInstance()
        global LONG, SHORT, ANY, BUY, SELL
        self.LONG, self.SHORT, self.ANY, self.BUY, self.SELL = LONG, SHORT, ANY, BUY, SELL
        self.__config = config

        if(configDB != None):
            self.__dao = storage.getInstance(configDB)

        self.__sink = sink \
            .buffer_with_count(1, self.__config['interval']) \
            .map(lambda s: s[0]) \
            .map(lambda s: self.__mapInput(s)) \
            

    def __mapInput(self, s):
        self.__temporalOrder = {}
        for key, value in s.items():
            self.__temporalOrder[key] = value
        return s

    def sendOrder(self, configOrder):
        for key, value in configOrder.items():
            self.__temporalOrder[key] = value
        
        if(self.__dao != None):
            self.logging.debug('**************************')
            self.logging.debug(configOrder)

            if(configOrder['order'] == self.SELL):
                self.logging.debug('order Sell')
                self.__dao.SELL(self.__temporalOrder)
 
            elif(configOrder['order'] == self.BUY):
                self.logging.debug('order Buy')
                self.__dao.BUY(self.__temporalOrder)
 

    def __mapOutput(self, s):
        if(self.__dao != None):
            self.__dao.DATA_OUT_SAVE(s)
        return s

    def algoTraiding(self, sink):
        raise Exception("Funcion algoTraiding() no implementada!")

    def flowConnect(self):
        return self.algoTraiding(self.__sink)
        #self.algoTraiding(self.__sink) \
        #    .map(lambda s: self.__mapOutput(s))