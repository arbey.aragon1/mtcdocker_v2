import sys
sys.path.append('../shared')
import os
from pymongo import MongoClient
from dao_ma import DAOMA
from flask import Flask, redirect, url_for, request, render_template
import time
from al_0003_2mean import Al_0003_2Mean
from logger import LOGGER

if __name__ == "__main__":
    logging = LOGGER.getInstance()
    logging.debug('Start Oanda Service')
    
    receiver_data=DAOMA()
    flow=receiver_data.getFlow() 
    
    process = Al_0003_2Mean({'interval': 15},
        flow, {})

    newFlow = process.flowConnect()
    newFlow.subscribe(lambda s: logging.debug(s))

    #flow.subscribe(lambda s: logging.debug(s))

    receiver_data.startConsuming()
    while(True):
        time.sleep(60)