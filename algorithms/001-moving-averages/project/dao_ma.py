import sys
sys.path.append('../shared')
import pika
import json
from rx import Observable, Observer
import time
from logger import LOGGER

class DAOMA():
    __connection = None
    __channel = None
    __logging = None
    
    def __init__(self):
        self.__logging = LOGGER.getInstance()
        self.__connectionCreate()
        
    def __connectionCreate(self):
        for i in range(3):
            try:
                self.__connection = pika.BlockingConnection(
                pika.ConnectionParameters(
                    host='rabbitmq',
                    port=5672,
                    credentials=pika.PlainCredentials(username="admin", password="mypass")
                    )
                )
                break
            except:
                self.__connection = None
                self.__logging.debug('Hubo un error en la conexion '+str(i))
                time.sleep(2)

        if(self.__connection == None):
            self.__logging.debug('Tercer intento fallido')
            sys.exit()
        self.__channel = self.__connection.channel()
        self.__logging.debug('Conexion creada')
        
    def getFlow(self):
        self.__logging.debug('Obtiene el flujo')
        self.__channel.queue_declare(queue='hello')
        
        def mainStream(observer):
            self.__channel.basic_consume(
                lambda ch, method, properties, body: observer.on_next(json.loads(body)),
                queue='hello',
                no_ack=True)
        return Observable.create(mainStream)
        
    def startConsuming(self):
        self.__logging.debug(' [*] Waiting for messages. To exit press CTRL+C')
        self.__channel.start_consuming()
